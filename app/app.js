'use strict';

var underscore = angular.module('underscore', []);
        underscore.factory('_', function() {
            return window._; 
        });

angular.module('BoggleApp', [
  'ngRoute',
  'BoggleApp.boggle',
  'underscore',
  'BoggleService'
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) { 
  $routeProvider.when('/', {
    templateUrl: 'boggle/boggle.html',
    controller: 'BoggleController',
    controllerAs: 'vm'
  })
  .otherwise({redirectTo: '/'})
}]);
