'use strict';

angular.module('BoggleApp.boggle', ['ngRoute'])
    .controller('BoggleController', ['BoggleService', '$timeout', function (BoggleService, $timeout) {
        var vm = this;
        vm.startTheGame = startTheGame;
        vm.start = start;
        vm.confirm = confirm;
        vm.select = select;
        vm.makePlayers = makePlayers;
        vm.points = points;
        vm.multiPoints = multiPoints;


        init();
        makePlayers();
        shuffle();
        
        function init() {
            vm.disable = true;
            vm.gameStarted = false;
            vm.selectedLanguage = "English";
            vm.numberOfPlayers = 1;
            vm.gameTime = 30;
            vm.results = [];
            vm.currentPlayer = 0;
            vm.nextPlayer = 0;
            vm.showResults = false;
            vm.distribution = BoggleService.getDiceDistribution();
            vm.mytimeout;
            vm.dictionary = [];
        }


        function startTheGame() {
            vm.gameStarted = true;
            BoggleService.getDictionary(vm.selectedLanguage).then(function (data) {
                var dictionary = data.data.split('\n');               
                vm.dictionary = dictionary.map(function (e) { return e.trim(); });
            }, function (error) {
                console.log("Error on getting dictionary!");
                vm.dictionary = [];
            });
        }

        function shuffle() {
            vm.letters = [];
            vm.distribution = _.shuffle(vm.distribution);
            var i = 0;
            for (i = 0; i < 4; i++) {
                var j = 0;
                var a = [];
                for (j = 0; j < 4; j++) {
                    var letter = vm.distribution[i * 4 + j].charAt(Math.floor(Math.random() * 6));
                    a.push({
                        letter: letter,
                        row: i,
                        column: j
                    });
                }
                vm.letters.push(a);
            }
        }

        function select(s) {
            vm.word.text = vm.word.text + s.letter;
            vm.word.positions.push({ row: s.row, column: s.column });
        }

        function time() {
            vm.counter++;
            if (vm.counter < vm.gameTime) {
                vm.mytimeout = $timeout(time, 1000);
            }
            else {
                stop();
            }
        }

        function start() {
            vm.currentPlayer = vm.nextPlayer;

            vm.disable = false;
            vm.counter = 0;
            vm.score = 0;
            vm.word = {
                text: "",
                positions: []
            };
            vm.words = [];
            vm.mytimeout = $timeout(time, 1000);
        }

        function stop() {
            vm.disable = true;
            calculateResults();
            $timeout.cancel(vm.mytimeout);
        }

        function points(listOfWords) {
            return BoggleService.getScore(listOfWords);
        }

        function multiPoints(playersWords) {
            var result = BoggleService.getScores(playersWords);
            vm.duplicates = result.duplicates;
            return result.scores;
        }

        function confirm() {
            if (vm.word.text.length != 0) {
                vm.word = BoggleService.validateWord(vm.word, vm.dictionary, vm.words);
                if (vm.word.valid) {
                    vm.word.points = points([vm.word.text]);
                    vm.score += vm.word.points;
                }
                else {
                    vm.word.points = 0;
                }
                vm.words.push(vm.word);
                vm.word = {
                    text: "",
                    positions: []
                };
            }
        }

        function makePlayers() {
            vm.players = [];
            var i = 0;
            for (i = 0; i < vm.numberOfPlayers; i++) {
                vm.players.push({
                    number: i + 1,
                    name: "Player " + (i + 1).toString()
                });
            }
        }

        function calculateResults() {
            vm.results.push(
                {
                    index: vm.players[vm.currentPlayer].number - 1,
                    player: vm.players[vm.currentPlayer].name,
                    words: vm.words
                }
            );

            if (vm.nextPlayer == vm.numberOfPlayers - 1) {
                vm.showResults = true;
                var playersWords = vm.results.map(function (e) {
                    return e.words.filter(function (word) {
                        return word.valid;
                    }).map(function (w) {
                        return w.text;
                    });
                });
                vm.finalScores = multiPoints(playersWords);

                _.each(vm.results, function (player) {
                    _.each(player.words, function (word) {
                        if (_.contains(vm.duplicates, word.text)) {
                            word.duplicate = 'line-through';
                        }
                    });
                });
            }
            else {
                vm.nextPlayer += 1;
            }

        }

    }]);