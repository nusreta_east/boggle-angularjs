'use strict';

angular.module('BoggleService', ['ngRoute'])
    .factory('BoggleService', ['$http', function ($http) {
        var service = {
            getScore: getScore,
            getScores: getScores,
            validateWord: validateWord,
            getDictionary: getDictionary,
            getDiceDistribution: getDiceDistribution
        }

        // get one of valid letter distribution on dices
        function getDiceDistribution() {
            return [
                "RIFOBX", "PCMEDA", "KUTODN", "GENIVT",
                "ZDVNEA", "VEIEHF", "CATOIA", "OBAJMQ",
                "IBALVT", "MOSHRA", "PNIEHS", "EALRCS",
                "SPTLUE", "SEDNWO", "UGIRLW", "UGEYKL"
            ];
        }


        // get words from provided txt files
        function getDictionary(selectedLanguage) {
            var file = 'words-english.txt';
            if (selectedLanguage == "Deutch") file = 'words-deutch.txt';
            else if (selectedLanguage == "Francais") file = 'words-francais.txt';

            return $http.get(file);
        }

        //first function from asignment for scoring list of words
        function getScore(listOfWords) {
            var result = 0;
            _.each(listOfWords, function (word) {
                if (word.length < 3) result += 0;
                else if (word.length == 3 || word.length == 4) result += 1;
                else if (word.length == 5) result += 2;
                else if (word.length == 6) result += 3;
                else if (word.length == 7) result += 5;
                else result += 11;
            });
            return result;
        }

        //second function from assignment for multiplayer scoring
        function getScores(playersWords) {
            var scores = [];
            var allWords = _.flatten(playersWords, true);
            var duplicates = allWords.filter(function (value, index, self) {
                return (self.indexOf(value) !== index);
            });

            _.each(playersWords, function (playerWords) {
                var uniqueWords = _.difference(playerWords, duplicates);
                scores.push(getScore(uniqueWords));
            });
            return {
                scores: scores,
                duplicates: duplicates
            };
        }

        // validate word by all rules except is another player found the same word 
        function validateWord(word, dictionary, words) {
            word.valid = true;
            word.color = 'green';
            word.message = "Valid word!";

            if (word.text.length < 3) {
                word.valid = false;
                word.color = "red";
                word.message = "Word too short!";
            }
            else if (dictionary.indexOf(word.text.toLowerCase()) === -1) {
                word.valid = false;
                word.color = "red";
                word.message = "Word doesn't exist in dictionary!";
            }
            else if (!allNeighbours(word.positions)) {
                word.valid = false;
                word.color = "red";
                word.message = "Letters should be neighbours!";
            }
            else if (usedSameDice(word.positions)) {
                word.valid = false;
                word.color = "red";
                word.message = "Same dice twice in one word!";
            }
            else if (_.contains(_.map(words, function (e) { return e.text; }), word.text)) {
                word.valid = false;
                word.color = "red";
                word.message = "You've already picked that word!";
            }

            return word;
        }

        //helpers
        function usedSameDice(positions) {
            var unique = _.unique(positions);
            return !(unique.length === positions.length);
        }

        function getNeighbours(letterPosition) {
            return [
                { row: letterPosition.row - 1, column: letterPosition.column },
                { row: letterPosition.row + 1, column: letterPosition.column },
                { row: letterPosition.row - 1, column: letterPosition.column - 1 },
                { row: letterPosition.row + 1, column: letterPosition.column - 1 },
                { row: letterPosition.row - 1, column: letterPosition.column + 1 },
                { row: letterPosition.row + 1, column: letterPosition.column + 1 },
                { row: letterPosition.row, column: letterPosition.column - 1 },
                { row: letterPosition.row, column: letterPosition.column + 1 }
            ]
        }

        function allNeighbours(positions) {
            var result = true;
            var i = 1;
            for (i = 0; i < positions.length - 1; i++) {
                var neighbours = getNeighbours(positions[i]);
                var nextIsNeighbour = _.findWhere(neighbours, positions[i + 1]);              
                if (!nextIsNeighbour) {
                    result = false;
                    break;
                }
            }
            return result;
        }


        return service;
    }]);