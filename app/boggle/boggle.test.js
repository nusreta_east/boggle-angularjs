// from https://docs.angularjs.org/guide/unit-testing

describe('BoggleController', function() {
  beforeEach(module('BoggleApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('Boggle scoring functions', function() {
    var $scope, controller;

    beforeEach(function() {
      $scope = {};
      vm = $controller('BoggleController', { $scope: $scope });
    });

    it('First function should return score according to game rules', function() {
      var tests = ['catty', 'wampus', 'am', 'bumfuzzle', 'gardyloo', 'taradiddle', 'loo', 'snickersnee', 'widdershins', 'teabag', 'collywobbles', 'gubbins'];
      var score = vm.points(tests);
      expect(score).toEqual(80);
    });

    it('Second function should return score according to game rules', function() {      
      var Lucas = ['am', 'bibble', 'loo', 'malarkey', 'nudiustertian', 'quire', 'widdershins', 'xertz', 'bloviate', 'pluto'];
      var Clara = ['xertz', 'gardyloo', 'catty', 'fuzzle', 'mars', 'sialoquent', 'quire', 'lollygag', 'colly', 'taradiddle', 'snickersnee', 'widdershins', 'gardy'];
      var Klaus = ['bumfuzzle', 'wabbit', 'catty', 'flibbertigibbet', 'am', 'loo', 'wampus', 'bibble', 'nudiustertian', 'xertz'];
      var Raphael = ['bloviate', 'loo', 'xertz', 'mars', 'erinaceous', 'wampus', 'am', 'bibble', 'cattywampus'];
      var Tom  = ['bibble', 'loo', 'snickersnee', 'quire', 'am', 'malarkey'];

      var tests = [Lucas, Clara, Klaus, Raphael, Tom];
      var score = vm.multiPoints(tests);
      expect(score).toEqual([2, 51, 25, 22, 0]);
    });
    
  });
});