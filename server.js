var express = require('express');
var app     = express();

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/app/'));

//For avoidong Heroku $PORT error
app.get('/', function(request, response) {   
    response.sendFile(__dirname + 'index.html');    
})
.listen(app.get('port'), function() {
    console.log('App is running, server is listening on port ', app.get('port'));
});